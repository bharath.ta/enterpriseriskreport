var express = require('express');
var router = express.Router();
const puppeteer = require('puppeteer');
const { getHtmlView, uploadPdfToAws } = require('../services/index');
let reportData = new Map();

let gstFilingHistory = [
  {
    returnType : 'GSTR9C',
    financialYear : '2017-2018',
    taxPeriod : 'Annual',
    dateOfFiling : '31/01/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR9C',
    financialYear : '2017-2018',
    taxPeriod : 'Annual',
    dateOfFiling : '31/01/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR9C',
    financialYear : '2017-2018',
    taxPeriod : 'Annual',
    dateOfFiling : '31/01/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR9C',
    financialYear : '2017-2018',
    taxPeriod : 'Annual',
    dateOfFiling : '31/01/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR9C',
    financialYear : '2017-2018',
    taxPeriod : 'Annual',
    dateOfFiling : '31/01/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR9C',
    financialYear : '2017-2018',
    taxPeriod : 'Annual',
    dateOfFiling : '31/01/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR9C',
    financialYear : '2017-2018',
    taxPeriod : 'Annual',
    dateOfFiling : '31/01/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR9C',
    financialYear : '2017-2018',
    taxPeriod : 'Annual',
    dateOfFiling : '31/01/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR9C',
    financialYear : '2017-2018',
    taxPeriod : 'Annual',
    dateOfFiling : '31/01/2020',
    status : 'Filed'
  },
  {
    returnType : 'GSTR9C',
    financialYear : '2017-2018',
    taxPeriod : 'Annual',
    dateOfFiling : '31/01/2020',
    status : 'Filed'
  },
];

let payload = {
  nameOfOrganisation : 'HIND COMMERCIAL INDUSTRIAL CORPORATION',
  nameOfOrganisation : 'NA',
  dateOfIncorporation : 'NA',
  basicAddress : 'FMC FORTUNA,A7/ii,5,Kolkata , 234/3A A.J.C BOSE ROAD',
  place : 'KOLKATA',
  state : 'West Bengal',
  pincode : '700020',
  headOfficeAddress : 'NA',
  gstNum : '19AEEPB5725P1Z6',
  gstStatus : 'Active',
  legalName : 'RAMESH KUMAR BUBNA',
  tradeName : 'HIND COMMERCIAL INDUSTRIAL CORPORATION',
  sez : 'NA',
  cin : 'NA',
  companyNameStatus : 'NA',
  iecode : 'NA',
  iestatus : 'NA',
  pan : 'AEEPB5725P',
  tan : 'NA',
  typeOfOrganisation : 'NA',
  numOfEmployees : 'NA',
  annualTurnover : 'NA',
  exportPercentage : 'NA',
  natureOfBusiness : 'Nickel For Resale Purpose, Rubber (natural) Merchant Exporters',
  additionalBusiness : 'NA',
  keyStrengths : 'NA',
  keyWeakness : 'NA',
  financialStanding :'NA',
  accessToFinance1 :'NA',
  financialManagement1 : 'NA',
  insuranceRiskManagement1 :'NA',
  stabilityAndSurvival :'NA',
  revenuePotential :'NA',
  accessToFinance2 :'NA',
  financialManagement2 : 'NA',
  insuranceRiskManagement2 :'NA', 
  gstFilingHistory : gstFilingHistory,
}

/* GET home page. */
router.get('/viewpdf', async (req, res) => {
  console.log('inside index get route');
  return res.render('err.pug',payload);
});

/* Write to pdf */
router.post('/writePDF', (req,res) => {
  getHtmlView("err", payload, res);
});

router.get('/dummyRoute', async (req, res) => {
  console.log('inside index get route');
  let result = await printPDF(res);
  if(result && result.pdfUrl){
    console.log('inside get if',result.pdfUrl);
    res.status(200).json({ status : 'PDF Downloaded', URL : result.pdfUrl});
  }
});

module.exports = router;

let printPDF = async (res) => {
  const browser = await puppeteer.launch({ headless: true });
      const page = await browser.newPage();
      await page.goto('http://localhost:3000/getPdf', {waitUntil: 'networkidle0'});
      const pdf = await page.pdf({format: 'A4' });
      await browser.close();
      reportData.delete('username');
      return await uploadPdfToAws(pdf,`${new Date().getTime()}.pdf`,res);
}