/**
 * Constants enumerating the aws-sdk.
 * @const
 */
const AWS = require("aws-sdk");
const DEFAULT_REPORT_PATH = 'test-images/reports/';

require('dotenv').config();

AWS.config.update({
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_ACCESS_KEY
});

const s3 = new AWS.S3();
const INVOICE_BUCKET = "actyv-kyc-reports";

/**
 * Constants enumerating the html-pdf.
 * @const
 */
var pdf = require("html-pdf");

module.exports.uploadPdfToAws = (buffer , fileName, res) => {
  return new Promise((resolve) => {
    // Setting up S3 upload parameters
    const params = {
      Bucket: INVOICE_BUCKET,
      Key: `${DEFAULT_REPORT_PATH}${fileName}`, // File name you want to save as in S3
      Body: buffer
    };
    // Uploading files to the bucket
    s3.upload(params, function (err, data) {
      if (err) {
        console.log('Error in uploading S3 File' , err);
        resolve(false);
        return;
      }
      console.log(`File CSV uploaded successfully. ${data.Location}`);
      resolve({ pdfUrl : data.Location });
    });
  });
};